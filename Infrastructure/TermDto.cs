using System;
using Domain;

namespace Infrastructure
{
    internal class TermDto
    {
        public TermDto()
        {
        }

        public TermDto(Term term)
        {
            Name = term.Name;
            Synonyms = String.Join(",", term.Synonyms);
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Synonyms { get; set; }

        public Term ToTerm()
        {
            return new Term
            {
                Name = Name,
                Synonyms = Synonyms.Split(',')
            };
        }
    }
}