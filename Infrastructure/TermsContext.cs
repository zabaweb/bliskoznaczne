using System;
using Microsoft.EntityFrameworkCore;

namespace Infrastructure
{
    internal class TermsContext : DbContext
    {
        private const string ConnectionString = @"Server=localhost;Database=vimanet;Integrated Security=True";
        public TermsContext()
        {
            
        }
        
        [Obsolete("Only for migration purpose")]
        public TermsContext(DbContextOptions<TermsContext> ctx):base(ctx)
        {
            
        }
        
        public DbSet<TermDto> Terms { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TermDto>()
                .Property(b => b.Id)
                .ValueGeneratedOnAdd();
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(ConnectionString);
        }
    }
}