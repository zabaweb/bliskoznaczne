using System.Collections.Generic;
using System.Linq;
using Domain;

namespace Infrastructure
{
    internal class TermsBuilder
    {
        private readonly Dictionary<string, HashSet<string>> termsMap = new Dictionary<string, HashSet<string>>();

        public Term[] ToTerms()
        {
            return termsMap.Select(tm =>
                new Term
                {
                    Name = tm.Key,
                    Synonyms = tm.Value.ToArray()
                }).ToArray();
        }
        
        public TermsBuilder(Term[] dtos)
        {
            foreach (var dto in dtos)
            {
                ProcessDto(dto);
            }
        }

        private void ProcessDto(Term dto)
        {
            
            foreach (var synonym in dto.Synonyms)
            {
                AddSynonym( dto.Name,synonym);
                AddSynonym(synonym, dto.Name);
            }
        }

        private void AddSynonym(string termName, string synonym)
        {
            if (!termsMap.ContainsKey(termName))
            {
                termsMap.Add(termName, new HashSet<string>(new[] {synonym}));
                return;
            }

            termsMap.TryGetValue(termName, out var map);

            map.Add(synonym);
        }

    
    }
}