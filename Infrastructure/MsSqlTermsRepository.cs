using System.Linq;
using Domain;

namespace Infrastructure
{
    public class MsSqlTermsRepository : TermsRepository
    {
        public void Add(Term term)
        {
            using (var db = new TermsContext())
            {
                db.Terms.Add(new TermDto(term));
                db.SaveChanges();
            }
        }

        public Term[] GetAll()
        {
            Term[] dtos;
            using (var db = new TermsContext())
            {
                dtos = db.Terms.Select(x => x.ToTerm()).ToArray();
            }

            var builder = new TermsBuilder(dtos);
            return builder.ToTerms();
        }
        }
}