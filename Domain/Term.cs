using System;

namespace Domain
{
    public class Term
    {
        private string _name;

        public string Name
        {
            get => _name;
            set
            {
                if (value == null)
                {
                    throw new Exception("Invalid term. Value cannot be empty");
                }

                if (value.Contains(","))
                {
                    throw new Exception("Invalid term. Provide name without comma ','");
                }

                _name = value.ToLower();
            }
        }

        public string[] Synonyms { get; set; }
    }
}