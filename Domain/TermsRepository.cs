namespace Domain
{
    public interface TermsRepository
    {
        void Add(Term term);
        Term[] GetAll();
    }
}