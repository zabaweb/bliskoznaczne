import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { AllTerms } from './components/AllTerms';
import { AddTerm } from './components/AddTerm';

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={AllTerms} />
        <Route path='/add' component={AddTerm} />
      </Layout>
    );
  }
}
