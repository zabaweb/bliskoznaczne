import React, { Component } from "react";

export class AddTerm extends Component {
  static displayName = AddTerm.name;

  constructor() {
    super();
    this.state = {
      term: "",
      synonyms: ""
    };
  }

  handleChange = stateFieldName => event => {
    const o = {};
    o[stateFieldName] = event.target.value;
    this.setState(o);
  };

  getTerm = () => ({
    name: this.state.term,
    synonyms: this.state.synonyms.split(",").map(x => x.trim())
  });

  isTermValid = () =>{
    const term = this.getTerm();

    if(term.name.indexOf(",") >= 0){
      return false;
    }

    if(term.name.length === 0){
      return false;
    }

    if(term.synonyms.some(s => s.trim().length === 0)){
      return false;
    }

    return true;
  }

  save = () => {
    fetch("api/Terms/Add", {
      method: "POST",
      mode: "cors",
      cache: "no-cache",
      credentials: "same-origin",
      headers: {
        "Content-Type": "application/json"
      },
      referrer: "no-referrer",
      body: JSON.stringify(this.getTerm())
    }).then(response => {
      if (response.status >= 200 && response.status < 300) {
        this.props.history.push("/");
      } else {
        alert("Something goes wrong");
      }
    });
  };

  render() {
    return (
      <div>
        <h3>Add Term</h3>

        <div className="input-group mb-3">
          <div className="input-group-prepend">
            <span className="input-group-text" id="term-addon">
              Term
            </span>
          </div>
          <input
            value={this.state.term}
            onChange={this.handleChange("term")}
            type="text"
            className="form-control"
            placeholder="Term"
            aria-label="Term"
            aria-describedby="term-addon"
          />
        </div>

        <div className="input-group mb-3">
          <div className="input-group-prepend">
            <span className="input-group-text" id="synonyms-addon">
              Synonym List
            </span>
          </div>
          <input
            value={this.state.synonyms}
            onChange={this.handleChange("synonyms")}
            type="text"
            className="form-control"
            placeholder="Synonym List"
            aria-label="Synonym List"
            aria-describedby="synonyms-addon"
          />
        </div>

        <button 
        className="btn btn-primary" 
        onClick={this.save}
        disabled={!this.isTermValid()}
        >
          Save
        </button>
      </div>
    );
  }
}
