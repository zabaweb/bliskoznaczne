import React, { Component } from "react";

export class AllTerms extends Component {
  static displayName = AllTerms.name;

  constructor(props) {
    super(props);
    this.state = { terms: [] };

    fetch("api/Terms/Data")
      .then(response => response.json())
      .then(data => {
        this.setState({ terms: data });
      });
  }

  render() {
    console.log(this.state.terms);
    return (
      <div>
        <h3>Terms</h3>
        {this.state.terms.map(term => (
          <div className="card" key={term.name}>
            <div className="card-body">
              <h5 className="card-title">{term.name}</h5>
              <p className="card-text">
                {term.synonyms.map(s => (
                  <span key={s} className="badge badge-secondary">
                    {s}
                  </span>
                ))}{" "}
              </p>
            </div>
          </div>
        ))}
      </div>
    );
  }
}
