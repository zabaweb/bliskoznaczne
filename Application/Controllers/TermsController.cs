using System.Collections.Generic;
using Domain;
using Microsoft.AspNetCore.Mvc;

namespace Application.Controllers
{
    [Route("api/[controller]")]
    public class TermsController : Controller
    {
        private readonly TermsRepository _termsRepository;

        public TermsController(TermsRepository termsRepository)
        {
            _termsRepository = termsRepository;
        }
        
        [HttpGet("[action]")]
        public IEnumerable<Term> Data()
        {
            return _termsRepository.GetAll();
        }
        
        [HttpPost("[action]")]
        public void Add([FromBody]Term term)
        {
           _termsRepository.Add(term);
        }
    }
}
